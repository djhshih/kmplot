# README #

`kmplot` is an R package for generating publication-quality Kaplan-Meier survival plots. Each plot will contain a table detailing the number of patients at risk across time points, in addition to essential statistics.

The API will likely undergo substantial changes.

### Dependencies ###

* R (>= 3.0)
* roxygen2 (>= 4.0, for building only)

### Build ###

Clone the repository, build the documentation with roxygen2, then install.

    $ git clone https://bitbucket.org/dshih/kmplot.git
    $ cd io
    $ R

    R> library(roxygen2)
    R> roxygenize()
    R> quit()

    $ R CMD INSTALL .

### Usage ###

Load the library and run the example.

    library(kmplot)
    example(kmplot)

See `?kmplot`.

